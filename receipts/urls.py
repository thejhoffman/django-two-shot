from django.urls import path
from .views import (
    ReceiptListView,
    ExpenseCategoryListView,
    AccountListView,
    ReceiptCreateView,
    ExpenseCategoryCreateView,
    AccountCreateView,
)


urlpatterns = [
    path(
        "",
        ReceiptListView.as_view(),
        name="home",
    ),
    path(
        "categories/",
        ExpenseCategoryListView.as_view(),
        name="list_categories",
    ),
    path(
        "accounts/",
        AccountListView.as_view(),
        name="list_accounts",
    ),
    path(
        "create/",
        ReceiptCreateView.as_view(),
        name="create_receipt",
    ),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="create_category",
    ),
    path(
        "accounts/create/",
        AccountCreateView.as_view(),
        name="create_account",
    ),
]
